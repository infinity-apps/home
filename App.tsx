import React from "react";
import { Provider as StoreProvider } from 'react-redux';
import { Provider as PaperProvider } from 'react-native-paper';

import Index from './src';
import store from 'Store';
import { nativeBaseTheme, paperTheme } from 'Theme';

export default function App() {
  return (
    <StoreProvider store={ store }>
        <PaperProvider theme={ paperTheme }>
          <Index />
        </PaperProvider>
    </StoreProvider>
  );
}
