module.exports = function(api) {
  api.cache(true);
  return {
    presets: ['babel-preset-expo'],
    plugins: [
      ['react-native-reanimated/plugin'],
      [
        'module-resolver',
        {
          alias: {
            '@base': './',
            '@src': './src',
            'Api': './src/api',
            'ApiDevice': './src/api_device',
            'Components': './src/components',
            'Constants': './src/constants',
            'I18n': './src/i18n',
            'Navigation': './src/navigation',
            'Screens': './src/screens',
            'Store': './src/store',
            'Theme': './src/theme',
            'Types': './src/types',
            'Utils': './src/utils',
          },
        },
      ],
      [
        'module:react-native-dotenv',
        {
          // 'moduleName': '@env',
          // 'path': '.env.shared',
          // 'blacklist': null,
          // 'whitelist': null,
          // 'safe': false,
          // 'allowUndefined': false
        }
      ]
    ],
  };
};
