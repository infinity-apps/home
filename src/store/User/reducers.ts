import { USER_AUTOLOGIN, USER_CODE_SUCCESS, USER_LOGIN_REQUEST, USER_LOGIN_SUCCESS } from './types';

export const initialState = {
  userId: null,
  code: null,
};

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN_REQUEST:
      return { ...state };
    case USER_LOGIN_SUCCESS:
    case USER_AUTOLOGIN:
      const { userId } = action.payload;
      return { ...state, userId };
    default:
      return state;
  }
};

export default userReducer;
