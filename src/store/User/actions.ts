import { LOADER_GLOBAL_SHOW, LOADER_GLOBAL_HIDE } from 'Store/Loader/types';
import {NOTIFICATION_SHOW, NOTIFICATION_TYPE_ERROR, NOTIFICATION_TYPE_INFO} from 'Store/Notification/types';
import { request } from 'Utils';
import * as SecureStore from 'expo-secure-store';

import {USER_LOGIN_REQUEST, USER_LOGIN_SUCCESS, USER_STATUS_RELOGIN_REQUIRED} from './types';
import {notificationAction} from "Store/Notification/actions";

export const userLoginAction = async (dispatch, data) => {
  dispatch({ type: LOADER_GLOBAL_SHOW });
  dispatch({ type: USER_LOGIN_REQUEST });

  return request(
    'user/login',
    data,
    async ({ data }) => {
      const { user_id: userId } = data;

      await SecureStore.setItemAsync('user_id', userId);

      return dispatch({
        type: USER_LOGIN_SUCCESS,
        payload: { userId },
      });
    },
    ({ code }) => {
      notificationAction(dispatch, { code, type: NOTIFICATION_TYPE_ERROR });
    },
    () => {
      return dispatch({
        type: LOADER_GLOBAL_HIDE,
      });
    }
  );
};