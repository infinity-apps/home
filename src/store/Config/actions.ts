import * as FileSystem from 'expo-file-system';
import { getLocales } from 'expo-localization';
import * as SecureStore from 'expo-secure-store';

import { configTranslationsRequest } from "Api";
import { CONFIG_ERROR, CONFIG_UPDATE } from './types';

export const configAction = async (dispatch, config) => {
  const storeConfig = await SecureStore.getItemAsync('config');
  const translationsDir = FileSystem.documentDirectory + 'i18n/';

  // If config is passed - pass everything
  if (config) {
    return dispatch({
      type: CONFIG_UPDATE,
      payload: config,
    });
  }

  // If config isn't passed & storeConfig is set - pass it & get translations from files
  if (storeConfig) {
    const parsedStoreConfig = JSON.parse(storeConfig);
    const translations = {};

    const mappedTranslations = parsedStoreConfig.availableTranslations.map(
      async (locale) =>
        await FileSystem.readAsStringAsync(translationsDir + locale).then(
          (content) => (translations[locale] = content)
        )
    );

    await Promise.all(mappedTranslations);
    parsedStoreConfig.translations = translations;

    return dispatch({
      type: CONFIG_UPDATE,
      payload: parsedStoreConfig,
    });
  }

  // If config isn't passed & storeConfig isn't set - set everything
  const { languageTag } = getLocales()[0];
  const locales = [...(languageTag !== 'en-US' ? [languageTag, 'en-US'] : [languageTag])];

  return configTranslationsRequest(
    { locales },
    async (res) => {
      const {
        data: { translations },
      } = res;

      const dirInfo = await FileSystem.getInfoAsync(translationsDir);
      if (!dirInfo.exists) {
        await FileSystem.makeDirectoryAsync(translationsDir, { intermediates: true });
      }

      const mappedTranslations = Object.keys(translations).map(async (locale) => {
        const file = translationsDir + locale;

        return await FileSystem.writeAsStringAsync(file, translations[locale])
          .then(async () => {
            const info = await FileSystem.getInfoAsync(file);
            return info.exists;
          })
          .catch((e) => console.log(e)); // @TODO catch err - prio 3/10
      });

      const createTranslations = await Promise.all(mappedTranslations);
      const translationsCreated = createTranslations.every((isCreated) => isCreated);

      if (translationsCreated) {
        const { languageCode, languageTag, textDirection } = getLocales()[0];

        const config = {
          locale: languageTag,
          lang: languageCode,
          textDirection,
          availableTranslations: Object.keys(translations),
        };

        await SecureStore.setItemAsync('config', JSON.stringify(config));

        config.translations = translations;

        return dispatch({
          type: CONFIG_UPDATE,
          payload: config,
        });
      } else {
        console.log('ERRR WITH CREATING TRANSLATION FILE');
      }
    },
    (err) =>
      dispatch({
        type: CONFIG_ERROR,
        err,
      })
  );
};
