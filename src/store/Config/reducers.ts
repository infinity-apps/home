import { CONFIG_UPDATE } from './types';

export const initialState = {
  locale: 'en-US',
  lang: 'en',
  textDirection: 'ltr',
  availableTranslations: null,
  translations: null,
};

const configReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case CONFIG_UPDATE:
      const config = typeof payload === 'string' ? JSON.parse(payload) : payload;
      return { ...state, ...config };
    default:
      return state;
  }
};

export default configReducer;
