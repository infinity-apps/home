import { NOTIFICATION_SHOW, NOTIFICATION_HIDE, NOTIFICATION_REMOVE, NOTIFICATION_TYPE_INFO } from './types';

export const initialState = {
  notifications: {},
};

const notificationReducer = (state = initialState, action) => {
  if (action.type === NOTIFICATION_SHOW) {
    const {
      code = code || Date.now(),
      type = NOTIFICATION_TYPE_INFO,
      text = code,
      debug = false,
    } = action.payload;

    return { ...state, notifications: { ...state.notifications, [code]: { type, text, debug, visible: true } } };
  } else if (action.type === NOTIFICATION_HIDE) {
    const { code } = action.payload;
    return { ...state, notifications: { ...state.notifications, [code]: { ...state.notifications[code], visible: false } } };
  } else if (action.type === NOTIFICATION_REMOVE) {
    const { code } = action.payload;
    const notifications = state.notifications;
    delete notifications[code];

    return { ...state, notifications: { ...notifications } };
  } else {
    return state;
  }
};

export default notificationReducer;
