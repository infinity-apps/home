import { NOTIFICATION_SHOW, NOTIFICATION_HIDE, NOTIFICATION_REMOVE } from './types';

export const notificationAction = async (dispatch, { show = true, code, type, text, debug }) => {
  dispatch({
    type: NOTIFICATION_SHOW,
    payload: { code, type, text, debug },
  });
};
