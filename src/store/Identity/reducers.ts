import { IDENTITY_REQUEST, IDENTITY_UPDATE, IDENTITY_ERROR } from './types';

export const initialState = {
  identity: '',
  _id: 'UNSET',
  _token: 'UNSET',
};

const identityReducer = (state = initialState, action) => {
  switch (action.type) {
    case IDENTITY_REQUEST:
      return { ...state };
    case IDENTITY_UPDATE:
      const { identity, _id, _token } = action.payload;
      return { ...state, identity, _id, _token };
    case IDENTITY_ERROR:
      return { ...state };
    default:
      return state;
  }
};

export default identityReducer;
