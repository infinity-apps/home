import { NOTIFICATION_SHOW, NOTIFICATION_TYPE_INFO } from 'Store/Notification/types';
import {
  USER_AUTOLOGIN,
  USER_STATUS_LOGGED_IN,
  USER_STATUS_RELOGIN_REQUIRED,
} from 'Store/User/types';
import { request } from 'Utils/Request';
import * as Device from 'expo-device';
import * as Network from 'expo-network';
import * as SecureStore from 'expo-secure-store';

import { identityVerifyRequest, identityCreateRequest } from "Api";
import { IDENTITY_REQUEST, IDENTITY_UPDATE, IDENTITY_ERROR } from './types';
import {notificationAction} from "Store/Notification/actions";
import {DEVICES_CONNECT_ERROR} from "Store/Devices/types";

export const identityAction = async (dispatch) => {
  let identity = await SecureStore.getItemAsync('identity');
  const _id = await SecureStore.getItemAsync('_id');
  const _token = await SecureStore.getItemAsync('_token');

  dispatch({ type: IDENTITY_REQUEST });

  if (identity && _id && _token) {
    return identityVerifyRequest(
        {},
        async ({ user_status }) => {
          if (user_status === USER_STATUS_LOGGED_IN) {
            const userId = await SecureStore.getItemAsync('user_id');

            dispatch({
              type: USER_AUTOLOGIN,
              payload: { userId },
            });
          } else if (user_status === USER_STATUS_RELOGIN_REQUIRED) {
            notificationAction(dispatch, { code: USER_STATUS_RELOGIN_REQUIRED, type: NOTIFICATION_TYPE_INFO });
          }

          return dispatch({
            type: IDENTITY_UPDATE,
            payload: { identity, _id, _token },
          });
        },
        (err) => {
          dispatch({
            type: IDENTITY_ERROR,
            err,
          });
        }
    );
  }

  const identityArr = {
    brand: Device.brand,
    model: Device.modelName,
    system: Device.osName,
    system_version: Device.osVersion,
    name: Device.deviceName,
    ipv4: await Network.getIpAddressAsync(),
  };
  identity = JSON.stringify(identityArr);

  return identityCreateRequest(
      { identity },
      async (res) => {
        const {
          data: { _id, _token },
        } = res;

        await SecureStore.setItemAsync('identity', identity);
        await SecureStore.setItemAsync('_id', _id);
        await SecureStore.setItemAsync('_token', _token);

        dispatch({
          type: IDENTITY_UPDATE,
          payload: { identity, _id, _token },
        });
      },
      (err) => {
        console.log(err);
        dispatch({
          type: IDENTITY_ERROR,
          err,
        });
      }
  );
};
