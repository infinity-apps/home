import { LOADER_GLOBAL_SHOW, LOADER_GLOBAL_HIDE } from './types';

export const initialState = {
  global: false,
};

const loaderReducer = (state = initialState, action) => {
  switch (action.type) {
    case LOADER_GLOBAL_SHOW:
      return { ...state, global: true };
    case LOADER_GLOBAL_HIDE:
      return { ...state, global: false };
    default:
      return state;
  }
};

export default loaderReducer;
