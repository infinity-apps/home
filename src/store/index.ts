import { combineReducers, createStore } from 'redux';

import { default as config } from './Config/reducers';
import { default as identity } from './Identity/reducers';
import { default as loader } from './Loader/reducers';
import { default as notification } from './Notification/reducers';
import { default as user } from './User/reducers';
import { default as devices } from './Devices/reducers';

export const reducers = combineReducers({
  config,
  identity,
  loader,
  notification,
  user,
  devices,
});

const store = createStore(reducers);

export default store;
