import { DEVICES_CONNECT_REQUEST, DEVICES_CONNECT_SUCCESS, DEVICES_CONNECT_ERROR } from './types';

export const initialState = {
  connectedDevices: [],
};

const devicesReducer = (state = initialState, action) => {
  switch (action.type) {
    case DEVICES_CONNECT_REQUEST:
      return { ...state };
    case DEVICES_CONNECT_SUCCESS:
      const { device } = action.payload;
      return { ...state, connectedDevices: [ ...state.connectedDevices, device ] };
    case DEVICES_CONNECT_ERROR:
      const { code } = action.payload;
      return { ...state, code };
    default:
      return state;
  }
};

export default devicesReducer;
