import { devicesConnectRequest } from "Api";
import { notificationAction } from "Store/Notification/actions";
import {
  DEVICES_CONNECT_ERROR,
  DEVICES_CONNECT_REQUEST,
  DEVICES_CONNECT_SUCCESS,
} from "./types";

export const devicesConnectAction = (dispatch, data) => {
  dispatch({ type: DEVICES_CONNECT_REQUEST });

  return devicesConnectRequest(
    data,
    async ({ data }) => {
      const { device } = data;

      return dispatch({
        type: DEVICES_CONNECT_SUCCESS,
        payload: { device },
      });
    },
    ({ code }) => {
      notificationAction(dispatch, { code, type: DEVICES_CONNECT_ERROR });
    }
  );
};
