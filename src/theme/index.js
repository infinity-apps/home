import {MD2LightTheme} from 'react-native-paper';

export const paperTheme = {
    ...MD2LightTheme
};