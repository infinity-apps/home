import {requestDevice} from "Utils/Request";

const networkPassData = async (data, successCallback, errorCallback) => {
    return requestDevice(
        'network_pass_data',
        data,
        async (res) => {
            successCallback(res);
        },
        (err) => {
            errorCallback(err);
        },
    );
};

export default networkPassData;