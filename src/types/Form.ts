export interface FormContainerPropsType {
  id?: String
  header?: String
  subHeader?: String
  fields: Array<string>|Array<FormFieldsType>
  submitAction: () => void
  loading: Boolean
  additionalContent?: JSX.Element
  validation: RegExp
  space?: Number
  submitText?: String
}

export interface FormContainerStateType {
  fieldsData: FormFieldsDataType | {}
  fieldsValues: FormFieldsValuesType | {}
}

export type FormFieldsType = {
  label: String
  name?: String
  regexp?: String|null
  required?: Boolean
  keyboardType?: String
  textContentType?: String
  autoComplete?: String
  secureTextEntry?: Boolean
};

export type FormFieldsDataType = {
  [key: string]: FormFieldsType & Array<{
    type: String
  }>
}

export type FormFieldsValuesType = {
  [key: string]: Array<{
    value: String
    validation: Boolean
  }>
}