import {request} from "Utils/Request";

const devicesInfoRequest = (data, successCallback, errorCallback) => {
    return request(
        'device/info',
        data,
        async (res) => {
            successCallback(res);
        },
        (err) => {
            errorCallback(err);
        },
    );
};

export default devicesInfoRequest;