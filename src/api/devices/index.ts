import devicesInfoRequest from "./info";
import devicesConnectRequest from "./connect";

export { devicesInfoRequest, devicesConnectRequest };
