import {request} from "Utils/Request";

const devicesConnectRequest = (data, successCallback, errorCallback) => {
    return request(
        'device/connect',
        data,
        async (res) => {
            successCallback(res);
        },
        (err) => {
            errorCallback(err);
        },
    );
};

export default devicesConnectRequest;