import {request} from "Utils/Request";

const configTranslationsRequest = (data, successCallback, errorCallback) => {
    return request(
        'config/translations',
        data,
        async (res) => {
            successCallback(res);
        },
        (err) => {
            errorCallback(err);
        },
    );
};

export default configTranslationsRequest;