import {request} from "Utils/Request";

const identityVerifyRequest = (data, successCallback, errorCallback) => {
    return request(
        'identity/verify',
        data,
        async (res) => {
            successCallback(res);
        },
        (err) => {
            errorCallback(err);
        },
    );
};

export default identityVerifyRequest;