import {request} from "Utils/Request";

const identityCreateRequest = (data, successCallback, errorCallback) => {
    return request(
        'identity/create',
        data,
        async (res) => {
            successCallback(res);
        },
        (err) => {
            errorCallback(err);
        },
    );
};

export default identityCreateRequest;