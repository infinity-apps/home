/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {Text} from 'react-native';
import {StyleSheet, View} from 'react-native';

import {fonts} from 'Constants/Fonts';
import HomeIcon from 'Components/Icons/Home';

const Logotype = ({ direction = 'row', showText = true }) => {
  const logoTextStyles = direction === 'column' ? {...styles.logoText, ...styles.logoTextColumn} : {...styles.logoText};

  return (
      <View flexDirection={direction} justifyContent="center" alignItems="center">
        <HomeIcon />
        {showText && (
            <Text style={logoTextStyles}>Home</Text>
        )}
      </View>
  );
};

const styles = StyleSheet.create({
  logoText: {
    marginTop: 3,
    marginLeft: 2,
    fontFamily: fonts.primaryBold,
    fontWeight: '600',
    fontSize: 21,
    letterSpacing: -2.3
  },
  logoTextColumn: {
    marginTop: -5,
    marginLeft: 0,
    fontSize: 16,
    letterSpacing: -1.7
  }
});

export default Logotype;
