import React, { useEffect, useState } from "react";
import { View } from "react-native";
import { Button, Headline, Subheading, TextInput } from "react-native-paper";

import { buttonColors } from "Constants/Colors";
import { FormType } from "Types/Form";

const Form = (props: FormType) => {
  const {
    loading,
    space,
    header,
    subHeader,
    submitText,
    additionalContent,
    fieldsData,
    fieldsValues,
    setFieldValues,
    buttonSubmitAction,
  } = props;
  const [fieldsArr, setFieldsArr] = useState([]);

  useEffect(() => {
    setFieldsArr([]);

    for (const [id, field] of Object.entries(fieldsData)) {
      const el: JSX.Element = (
        <TextInput
          key={id}
          label={field.label}
          mode="outlined"
          disabled={loading}
          autoCapitalize={field.autoCapitalize || "none"}
          keyboardType={field.keyboardType}
          textContentType={field.textContentType}
          autoComplete={field.autoComplete}
          secureTextEntry={field.secureTextEntry}
          error={!fieldsValues[id].validation}
          onChangeText={(value) => setFieldValues(id, { value })}
          style={{ width: "100%", marginTop: 20, marginBottom: 20 }}
        />
      );

      setFieldsArr((prevState) => [...prevState, el]);
    }
  }, [fieldsData, fieldsValues]);

  return (
    <View>
      <View>
        {header && <Headline>{header}</Headline>}
        {subHeader && <Subheading>{subHeader}</Subheading>}
        {fieldsArr}
      </View>
      <View>
        <Button
          mode="contained"
          color={buttonColors.primary}
          onPress={!loading && buttonSubmitAction}
          style={{ width: "100%" }}
          loading={loading}
        >
          {submitText || __("Submit")}
        </Button>
        {additionalContent}
      </View>
    </View>
  );
};

export default Form;
