import React, { PureComponent } from "react";

import {
  FormContainerPropsType,
  FormContainerStateType,
  FormFieldsDataType,
  FormFieldsType,
  FormFieldsValuesType,
} from "Types/Form";
import { regexp } from "Utils/Regexp";
import { removeSpaces } from "Utils/String";
import Form from "./Form.component";
import { FORM_DEFAULT_FIELDS_DATA } from "./Form.config";

export class FormContainer extends PureComponent<
  FormContainerPropsType,
  FormContainerStateType
> {
  static defaultProps = {
    loading: false,
    space: 2,
  };

  constructor(props: FormContainerPropsType) {
    super(props);

    const preparedFields = this._prepareFields();

    this.state = {
      fieldsData: preparedFields.fieldsData,
      fieldsValues: preparedFields.fieldsValues,
    };
  }

  _prepareFields = (): FormFieldsDataType & FormFieldsValuesType => {
    const { id, fields, header } = this.props;
    const formId = id || (header && removeSpaces(header));

    let fieldsData: FormFieldsDataType | Object = {};
    let fieldsValues: FormFieldsValuesType | Object = {};

    fields.forEach((field: string | FormFieldsType, i: number) => {
      let required = true;

      if (typeof field === "string") {
        required = !field.endsWith("?");
        field = field.endsWith("?") ? field.slice(0, -1) : field;
      }

      const fieldId =
        typeof field === "string"
          ? `${formId}__${field}__${i}`
          : `${formId}__custom__${i}`;

      switch (typeof field) {
        case "string":
          fieldsData[fieldId] = {
            ...FORM_DEFAULT_FIELDS_DATA["default"],
            ...FORM_DEFAULT_FIELDS_DATA[field],
            type: field,
            name: "",
            required,
          };
          break;
        case "object":
          required =
            field.required === undefined
              ? true
              : field.required === false
              ? false
              : true;
          fieldsData[fieldId] = {
            ...FORM_DEFAULT_FIELDS_DATA["default"],
            type: "custom",
            label: field.label,
            name: field.name || "",
            regexp: field.regexp || null,
            required,
          };
          break;
        default:
          break;
      }

      fieldsValues[fieldId] = {
        value: "",
        validation: true,
      };
    });

    return {
      fieldsData,
      fieldsValues,
    };
  };

  _validateField = (id: string, field: FormFieldsDataType): Boolean => {
    const { fieldsValues } = this.state;
    const { value } = fieldsValues[id];
    const required =
      field.required && value !== "" ? true : !field.required ? true : false;
    const fieldRegex = field.regexp?.split("|");
    const validationPassed: Array<boolean> = [];

    fieldRegex &&
      fieldRegex.forEach((regex: string) =>
        validationPassed.push(regexp[regex].test(value))
      );

    return (
      required &&
      (!validationPassed.length ||
        validationPassed.some((passed: boolean) => passed))
    );
  };

  setFieldValues = (
    id: string,
    payload: Array<{ value?: string; validation?: boolean }>
  ) => {
    this.setState(
      (prevState: FormContainerStateType): FormContainerStateType => ({
        fieldsValues: {
          ...prevState.fieldsValues,
          [id]: {
            ...prevState.fieldsValues[id],
            ...payload,
          },
        },
      })
    );
  };

  buttonSubmitAction = () => {
    const { submitAction } = this.props;
    const { fieldsData } = this.state;
    const validationArr:
      | {}
      | Array<{ [key: string]: { validation: boolean } }> = {};

    for (const [id, field] of Object.entries(fieldsData)) {
      const validation: Boolean = this._validateField(id, field);
      validationArr[id] = validation;
      this.setFieldValues(id, { validation });
    }

    const { fieldsValues } = this.state;

    if (Object.values(validationArr).every((passed) => passed)) {
      submitAction(fieldsValues);
    }
  };

  containerProps() {
    const { loading, space, header, subHeader, submitText, additionalContent } =
      this.props;

    const { fieldsData, fieldsValues } = this.state;

    return {
      loading,
      space,
      header,
      subHeader,
      submitText,
      additionalContent,
      fieldsData,
      fieldsValues,
    };
  }

  containerFunctions = {
    setFieldValues: this.setFieldValues.bind(this),
    buttonSubmitAction: this.buttonSubmitAction.bind(this),
  };

  render() {
    return <Form {...this.containerProps()} {...this.containerFunctions} />;
  }
}

export default FormContainer;
