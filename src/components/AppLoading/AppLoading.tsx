import React from 'react';
import {View} from 'react-native';
import {ActivityIndicator} from "react-native-paper";

const AppLoading = () => {
    return (
        <View style={{justifyContent: 'center', alignItems: 'center', flex: 1}}>
            <ActivityIndicator color="#212b00" />
        </View>
    );
};

export default AppLoading;
