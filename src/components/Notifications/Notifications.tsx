// import { colors } from 'Constants';
import { notificationAction } from 'Store/Notification/actions';
import {NOTIFICATION_REMOVE, NOTIFICATION_HIDE} from 'Store/Notification/types';
import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {View, StyleSheet, Alert, Text} from 'react-native';
import {Snackbar, Portal} from 'react-native-paper';
import {notificationColors} from "Constants/Colors";
import {fonts} from "Constants/Fonts";

const Notifications = () => {
  const dispatch = useDispatch();
  const notifications = useSelector(({ notification }) => notification.notifications);

  const onDismiss = (code) => {
    dispatch({
      type: NOTIFICATION_HIDE,
      payload: { code },
    });

    setTimeout(() => {
      dispatch({
        type: NOTIFICATION_REMOVE,
        payload: { code },
      });
    }, 2000);
  };

  const renderNotifications = Object.keys(notifications).map((code) => {
    const notification = notifications[code];
    const { type, text, visible } = notification;

    return (
      <Snackbar
          key={code}
          visible={visible}
          onDismiss={() => onDismiss(code)}
          duration={5000}
          style={{backgroundColor: notificationColors[type]}}
      >
        {text}
      </Snackbar>
      //   <PresenceTransition visible={visible} key={code} initial={{
      //     opacity: 0,
      //     translateY: -50
      //   }} animate={{
      //     opacity: 1,
      //     translateY: 0,
      //     transition: {
      //       duration: 250
      //     }
      //   }}>
      //     <Alert maxWidth="95%" alignSelf="center" flexDirection="row" variant="solid" status={type} key={code}>
      //       <VStack space={1} flexShrink={1} w="100%">
      //         <HStack flexShrink={1} alignItems="center" justifyContent="space-between">
      //           <HStack space={2} flexShrink={1} alignItems="center">
      //             <Alert.Icon />
      //             <Text fontSize="md" fontWeight="medium" flexShrink={1} color="lightText">
      //               {text}
      //             </Text>
      //           </HStack>
      //         </HStack>
      //       </VStack>
      //     </Alert>
      // </PresenceTransition>
    );
  });

  return <Portal>{renderNotifications}</Portal>;
};

export default Notifications;
