export const CONNECT_QUICK_AUTH_STEPS = [
  {
    name: "default",
    step: 0,
  },
  {
    name: "list",
    step: 1,
  },
  {
    name: "connect-password",
    step: 2,
    returnStep: 1,
  },
  {
    name: "autoconnect",
    step: 3,
    returnStep: 1,
  },
  {
    name: "login-wifi--1-select-network",
    step: 4,
    returnStep: 1,
  },
  {
    name: "login-wifi--2-password",
    step: 5,
    returnStep: 4,
  },
];
