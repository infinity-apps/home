/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React, { useState, useEffect } from 'react';
import { View, Button, StyleSheet, Image, Text } from 'react-native';
import { IconButton, List, Surface } from 'react-native-paper';
import { BarCodeScanner } from 'expo-barcode-scanner';

import DeviceIcon from 'Components/Icons/Device';
import BackArrowIcon from 'Components/Icons/BackArrow';

const ConnectQrCode = ({ goBack }) => {
  const [hasScannerPermission, setHasScannerPermission] = useState(null);
  const [qrScanned, setQrScanned] = useState(false);

  useEffect(() => {
    (async () => {
      const {status} = await BarCodeScanner.requestPermissionsAsync();
      setHasScannerPermission(status === 'granted');
    })();
  });

  if (hasScannerPermission === null) {
    return <Text>Requesting for camera permission</Text>;
  }
  if (hasScannerPermission === false) {
    return <Text>No access to camera</Text>;
  }

  return (
    <View>
      <View
        w="100%"
        h="20%"
      >
        <List.Item
          title={props => 
            <Surface style={styles.topTextContainer}>
              <Text style={styles.topText}>Scan QR code on the device's case.</Text>
            </Surface>
          } left={props =>
            <Image style={styles.topImage} source={{ uri: 'https://im3.ezgif.com/tmp/ezgif-3-4c52966430.gif' }}/> // @TODO change
          }
        />
      </View>
      <View
        w="100%"
        h="75%"
      >
        <BarCodeScanner
          // onBarCodeScanned={qrScanned ? undefined : handleBarCodeScanned}
          style={styles.scanner}
        />
        {qrScanned && <Button title={'Tap to Scan Again'} onPress={() => setQrScanned(false)} />}
      </View>
      <View h="5%" w="100%">
        <View justifyContent="center" alignItems="center">
          {goBack &&
            <IconButton
              icon={({ size, color }) => (<BackArrowIcon />)}
              size={40}
              onPress={() => goBack()}
            /> }
        </View>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  topTextContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#6ecab3',
    width: '150%',
    height: 88,
    left: '-50%',
    zIndex: -1,
    borderTopLeftRadius: 50,
    borderBottomLeftRadius: 50
  },
  topText: {
    color: 'white',
    paddingLeft: 100,
    paddingRight: 25,
    fontSize: 20,
    textAlign: 'right'
  },
  topImage: {
    width: 100,
    height: 100,
    backgroundColor: 'white',
    borderRadius: 100
  },
  container: {

  },
  scanner: {
    ...StyleSheet.absoluteFillObject
  }
});

export default ConnectQrCode;
