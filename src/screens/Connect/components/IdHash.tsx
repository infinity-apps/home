/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from "react";

import Form from "Components/Form";
import { Text, View } from "react-native";

const ConnectIdHash = () => {
  return (
    <View>
      <Text fontSize="md">
        {__(
          "Connect by entering ID and Hash visible on the bottom of the device."
        )}
      </Text>
      {/*<Divider />*/}
      <Form
        id="connect-id-hash"
        fields={["id", "hash"]}
        submitAction={() => console.log("abc")}
      />
    </View>
  );
};

export default ConnectIdHash;
