import React, { useEffect, useRef, useState } from "react";

import { Ionicons } from "@expo/vector-icons";
import AppLoading from "Components/AppLoading";
import Form from "Components/Form";
import WifiIcon from "Components/Icons/Wifi";
import { Animated, Easing, ScrollView, Text, View } from "react-native";
import { List, Surface, TouchableRipple } from "react-native-paper";
import WifiManager from "react-native-wifi-reborn";
import { useDispatch } from "react-redux";
import { CONNECT_QUICK_AUTH_STEPS } from "Screens/Connect/Connect.config";
import { notificationAction } from "Store/Notification/actions";
import { NOTIFICATION_TYPE_ERROR } from "Store/Notification/types";
import {networkPassData} from "ApiDevice/network";

const ConnectQuickAuth = ({
  quickAuth,
  isConnecting,
  currSSID,
  quickAuthModalState,
  setQuickAuthModalState,
  searchedDevices,
}) => {
  const dispatch = useDispatch();

  const selectedNetwork = useRef({});
  const selectedDevice = useRef({});
  const [wifiNetworks, setWifiNetworks] = useState([]);

  const refreshAnimInitialVal = new Animated.Value(0);

  useEffect(() => {
    if (!isConnecting) {
      Animated.timing(refreshAnimInitialVal).stop();
      return;
    }

    Animated.loop(
        Animated.timing(refreshAnimInitialVal, {
          toValue: 1,
          duration: 1500,
          easing: Easing.bezier(1, 0.5, 0, 0.5),
          useNativeDriver: true,
        })
    ).start();
  }, [isConnecting]);

  const refreshAnimVal = refreshAnimInitialVal.interpolate({
    inputRange: [0, 1],
    outputRange: ["0deg", "360deg"],
  });

  const connectDevice = async (device) => {
    const { ssid, password } = device;
    selectedDevice.current = device;

    if (password) {
      setQuickAuthModalState(CONNECT_QUICK_AUTH_STEPS[3]);
      // devicesConnectAction(dispatch, { ssid, device }); // Causes crash

      try {
        await WifiManager.connectToProtectedSSID(ssid, password, false, false);
        await wifiNetworksAction();
        setQuickAuthModalState(CONNECT_QUICK_AUTH_STEPS[4]);
      } catch (error) {
        selectedDevice.current = {};
        setQuickAuthModalState(CONNECT_QUICK_AUTH_STEPS[1]);

        notificationAction(dispatch, {
          text: __("Error when connecting to the device. %s", error),
          type: NOTIFICATION_TYPE_ERROR,
        });
      }
    } else {
      setQuickAuthModalState(CONNECT_QUICK_AUTH_STEPS[2]);
    }
  };

  const onSubmitDevicePasswordForm = async (fields) => {
    const password = Object.values(fields)[0].value;
    const device = {...selectedDevice.current, password};

    await connectDevice(device);
  };

  const wifiNetworksAction = async () => {
    try {
      const wifiList = await WifiManager.reScanAndLoadWifiList();
      const wifiNetworks = wifiList.filter(
          ({SSID, frequency}) => {
            const {ssid: selectedDeviceSSID} = selectedDevice.current;
            let filterPassed = !SSID.startsWith("IHOME") && SSID !== "(hidden SSID)";

            if (selectedDeviceSSID.endsWith('_2.4G') && frequency > 2500) {
              filterPassed = false;
            }

            return filterPassed;
          }
      );

      if (wifiNetworks.length > 0) {
        const networks = wifiNetworks
            .map(({BSSID, SSID, level}) => ({
              BSSID,
              SSID,
              level,
            }))
            .sort((a, b) => b.level - a.level);

        setWifiNetworks(networks);
      } else {
        notificationAction(dispatch, {
          text: __("Wi-Fi networks not found."),
          type: NOTIFICATION_TYPE_WARNING,
        });
      }
    } catch (error) {
      notificationAction(dispatch, {
        text: __("Error when getting networks. Details: %s", error),
        type: NOTIFICATION_TYPE_ERROR,
      });
    }
  };

  const onSubmitPassNetworkDataForm = async (fields) => {
    selectedNetwork.current.password = Object.values(fields)[0].value;

    const { ssid, password } = selectedNetwork.current;

    await networkPassData({
      ssid,
      password
    },
    res => {
      alert('RES ' + JSON.stringify(res));
      setQuickAuthModalState(CONNECT_QUICK_AUTH_STEPS[0].step);
    }, err => {
      alert('ERR ' + JSON.stringify(err));
    });
  };

  const renderSearchedDevices = () => {
    return (
      <>
        {searchedDevices &&
          Object.keys(searchedDevices).map((ssid) => {
            const device = searchedDevices[ssid];
            const preparedLevel =
              device.level < -30 ? 100 + device.level + 15 : 100;

            return (
              <List.Item
                key={ssid}
                title={ssid}
                description={device.name}
                left={(props) => (
                  <List.Icon
                    {...props}
                    icon={() => {
                      if (
                        selectedDevice.current?.ssid === ssid &&
                        quickAuthModalState.step ===
                          CONNECT_QUICK_AUTH_STEPS[3].step
                      ) {
                        return <AppLoading />;
                      } else {
                        return (
                          <>
                            <WifiIcon />
                            <Text
                              style={{
                                position: "absolute",
                                right: -8,
                                bottom: -8,
                                fontSize: 12,
                                fontWeight: "bold",
                              }}
                            >
                              {preparedLevel + "%"}
                            </Text>
                          </>
                        );
                      }
                    }}
                  />
                )}
                onPress={() => connectDevice({ssid, ...device})}
              />
            );
          })}
      </>
    );
  };

  const renderWifiNetworks = () => {
    return (
      <View style={{ maxHeight: "80%", marginTop: 25 }}>
        <ScrollView>
          {wifiNetworks.length ? (
            wifiNetworks.map(({ BSSID, SSID, level }) => {
              const preparedLevel = level < -30 ? 100 + level + 15 : 100;

              return (
                <List.Item
                  key={BSSID}
                  title={SSID}
                  style={[
                    SSID === currSSID ? { backgroundColor: "#eaeaea" } : {},
                  ]}
                  left={(props) => (
                    <List.Icon
                      {...props}
                      icon={() => {
                        return (
                          <>
                            <WifiIcon />
                            <Text
                              style={{
                                position: "absolute",
                                right: -8,
                                bottom: -8,
                                fontSize: 12,
                                fontWeight: "bold",
                              }}
                            >
                              {preparedLevel + "%"}
                            </Text>
                          </>
                        );
                      }}
                    />
                  )}
                  onPress={() => {
                    selectedNetwork.current.ssid = SSID;
                    setQuickAuthModalState(CONNECT_QUICK_AUTH_STEPS[5]);
                  }}
                />
              );
            })
          ) : (
            <AppLoading />
          )}
        </ScrollView>
      </View>
    );
  };

  return (
    <List.Section>
      <View style={{ flexDirection: "row", marginLeft: "auto" }}>
        {quickAuthModalState.step === CONNECT_QUICK_AUTH_STEPS[1].step ? (
          <TouchableRipple
            onPress={quickAuth}
            rippleColor="rgba(0, 0, 0, .2)"
            style={{ padding: 10 }}
          >
            <Animated.View style={{ transform: [{ rotate: refreshAnimVal }] }}>
              <Ionicons name="md-refresh-sharp" size={24} color="black" />
            </Animated.View>
          </TouchableRipple>
        ) : quickAuthModalState.step !== CONNECT_QUICK_AUTH_STEPS[1].step ? (
          <TouchableRipple
            onPress={() =>
              setQuickAuthModalState(
                CONNECT_QUICK_AUTH_STEPS[quickAuthModalState.returnStep]
              )
            }
            rippleColor="rgba(0, 0, 0, .2)"
            style={{ padding: 10 }}
          >
            <Ionicons name="return-down-back-outline" size={24} color="black" />
          </TouchableRipple>
        ) : (
          <></>
        )}
        <TouchableRipple
          onPress={() => setQuickAuthModalState(CONNECT_QUICK_AUTH_STEPS[0])}
          rippleColor="rgba(0, 0, 0, .2)"
          style={{ padding: 10 }}
        >
          <Ionicons name="close-outline" size={24} color="black" />
        </TouchableRipple>
      </View>
      {quickAuthModalState.step === CONNECT_QUICK_AUTH_STEPS[1].step ||
      quickAuthModalState.step === CONNECT_QUICK_AUTH_STEPS[3].step ? (
        <View>
          <List.Subheader>
            <Text>
              {__(
                "Found %s devices",
                searchedDevices ? Object.keys(searchedDevices).length : 0
              )}
            </Text>
          </List.Subheader>
          <Surface style={{ height: 20 }} elevation={0} />
          {renderSearchedDevices()}
        </View>
      ) : quickAuthModalState.step === CONNECT_QUICK_AUTH_STEPS[2].step ? (
        <Form
          fields={["password"]}
          header={__("Connect manually")}
          subHeader={__(
            "You are too far from the device.\nEnter the password to connect. You can find it on the bottom of the device."
          )}
          submitText={__("Connect")}
          submitAction={(fields) => onSubmitDevicePasswordForm(fields)}
        /> // @TODO - do better
      ) : quickAuthModalState.step === CONNECT_QUICK_AUTH_STEPS[4].step ? (
        <View>
          <Text fontSize="md">
            {__("Select the home network your device will connect to.")}
          </Text>
          {renderWifiNetworks()}
        </View>
      ) : quickAuthModalState.step === CONNECT_QUICK_AUTH_STEPS[5].step ? (
        <Form
          fields={[
            {
              label: "Password",
              keyboardType: "default",
              textContentType: "none",
              autoComplete: "off",
              secureTextEntry: false,
            },
          ]}
          header={__("Wi-Fi password")}
          subHeader={__(
            "Password for the network: %s",
            selectedNetwork.current.ssid
          )}
          submitText={__("Pass network data")}
          submitAction={(fields) => onSubmitPassNetworkDataForm(fields)}
        /> // @TODO - do better
      ) : (
        <></>
      )}
    </List.Section>
  );
};

export default ConnectQuickAuth;
