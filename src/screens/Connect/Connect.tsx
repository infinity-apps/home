import React from "react";
import { PermissionsAndroid, Platform, View } from "react-native";
import { IconButton, Modal, Portal, Text } from "react-native-paper";
import WifiManager from "react-native-wifi-reborn";
import { useDispatch, useSelector } from "react-redux";
// import {Container, Flex, Text, HStack, VStack, Center, PresenceTransition} from 'native-base';
import { devicesInfoRequest } from "Api";
import AppLoading from "Components/AppLoading";
import IdHash from "./components/IdHash";
import QrCode from "./components/QrCode";
import QuickAuth from "./components/QuickAuth";
import HomeIcon from "Components/Icons/Home";
import PinCodeIcon from "Components/Icons/PinCode";
import QrScanIcon from "Components/Icons/QrScan";
import { fonts } from "Constants/Fonts";
import { notificationAction } from "Store/Notification/actions";
import {
  NOTIFICATION_TYPE_ERROR,
  NOTIFICATION_TYPE_WARNING,
} from "Store/Notification/types";
import { CONNECT_QUICK_AUTH_STEPS } from "./Connect.config";

const Connect = ({ navigation }) => {
  const dispatch = useDispatch();
  // const connectedDevices = useSelector(
  //   ({ devices }) => devices.connectedDevices
  // );

  const [searchedDevices, setSearchedDevices] = React.useState({});
  const [isConnecting, setIsConnecting] = React.useState(false);
  const [currSSID, setCurrSSID] = React.useState("");
  // const [wifiDevices, setWifiDevices] = React.useState([]);
  // const [isConfigManOpen, setIsConfigManOpen] = React.useState(false);
  const [isIdHashModalOpen, setIsIdHashModalOpen] = React.useState(false);
  const [isQrCodeModalOpen, setIsQrCodeModalOpen] = React.useState(false);
  const [quickAuthModalState, setQuickAuthModalState] = React.useState(
    CONNECT_QUICK_AUTH_STEPS[0]
  );

  const quickAuth = async () => {
    if (isConnecting) return;

    setIsConnecting(true);

    if (Platform.OS === "android") {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: __("Location permission is required for WiFi connections."),
          message: __(
            "This app needs location permission as this is required  to scan for wifi networks."
          ),
          buttonNegative: __("DENY"),
          buttonPositive: __("ALLOW"),
        }
      );

      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        try {
          const currSSID = await WifiManager.getCurrentWifiSSID();
          const wifiList = await WifiManager.reScanAndLoadWifiList();
          const wifiDevices = wifiList.filter(({ SSID }) =>
            SSID.startsWith("IHOME-")
          );

          if (wifiDevices.length > 0) {
            const devices = wifiDevices.map(({ SSID, level }) => ({
              SSID,
              level,
            }));

            devicesInfoRequest(
              { devices },
              ({ data }) => {
                const { devices } = data;
                setCurrSSID(currSSID);
                setSearchedDevices(devices);
                setQuickAuthModalState(CONNECT_QUICK_AUTH_STEPS[1]);
              },
              ({ code }) => {
                notificationAction(dispatch, {
                  text: __("Error when getting device info. Details: %s", code),
                  type: NOTIFICATION_TYPE_ERROR,
                });
              }
            );
          } else {
            notificationAction(dispatch, {
              text: __("No devices found."),
              type: NOTIFICATION_TYPE_WARNING,
            });
          }
        } catch (error) {
          notificationAction(dispatch, {
            text: __("Error when getting device info. Details: %s", error),
            type: NOTIFICATION_TYPE_ERROR,
          });
        } finally {
            setIsConnecting(false);
        }
      } else {
        notificationAction(dispatch, {
          text: __("You need the location permission to search devices."),
          type: NOTIFICATION_TYPE_ERROR,
        });

        setIsConnecting(false);
      }
    } else if (Platform.OS === "ios") {
      // @TODO do for iOS
    } else {
      // @TODO do for other (desktop etc.)
    }
  };

  return (
    <View
      style={{
        justifyContent: "center",
        alignItems: "center",
        height: "100%",
        padding: 20,
      }}
    >
      <Text style={{ fontFamily: fonts.primary, fontSize: 18 }}>
        {__("Not connected yet?")}
      </Text>
      <IconButton
        icon={({ size, color }) =>
          !isConnecting ? <HomeIcon /> : <AppLoading />
        }
        size={60}
        style={{ marginTop: 30, backgroundColor: "white", elevation: 8 }}
        onPress={quickAuth}
      />
      <View
        style={{
          width: "100%",
          alignItems: "center",
          marginTop: 25,
          marginBottom: 25,
        }}
      >
        <View
          style={{
            width: "50%",
            height: 1,
            backgroundColor: "#ddd",
            position: "absolute",
            top: "55%",
          }}
        />
        <Text
          style={{
            fontFamily: fonts.primary,
            paddingRight: 10,
            paddingLeft: 10,
            backgroundColor: "white",
            overflow: "hidden",
          }}
        >
          or
        </Text>
      </View>
      <View style={{ flexDirection: "row" }}>
        <View style={{ marginRight: 15 }}>
          <IconButton
            icon={({ size, color }) => <QrScanIcon />}
            size={50}
            style={{ backgroundColor: "white", elevation: 8 }}
            onPress={() => setIsQrCodeModalOpen(!isQrCodeModalOpen)}
          />
          <View>
            <Text
              style={{
                fontFamily: fonts.primary,
                marginTop: 10,
                textAlign: "center",
              }}
            >
              QR Code
            </Text>
          </View>
        </View>
        <View style={{ marginLeft: 15 }}>
          <IconButton
            icon={({ size, color }) => <PinCodeIcon />}
            size={50}
            style={{ backgroundColor: "white", elevation: 8 }}
            onPress={() => setIsIdHashModalOpen(!isIdHashModalOpen)}
          />
          <View>
            <Text
              style={{
                fontFamily: fonts.primary,
                marginTop: 10,
                textAlign: "center",
              }}
            >
              {__("ID + Password")}
            </Text>
          </View>
        </View>
      </View>
      <Portal>
        <Modal
          visible={isIdHashModalOpen}
          onDismiss={() => setIsIdHashModalOpen(!isIdHashModalOpen)}
          contentContainerStyle={{
            backgroundColor: "white",
            padding: 15,
            paddingTop: 45,
            paddingBottom: 45,
          }}
        >
          <IdHash />
        </Modal>
        <Modal
          visible={isQrCodeModalOpen}
          onDismiss={() => setIsQrCodeModalOpen(!isQrCodeModalOpen)}
          contentContainerStyle={{
            backgroundColor: "white",
            padding: 15,
            paddingTop: 45,
            paddingBottom: 45,
          }}
        >
          <QrCode
            goBack={() => setIsQrCodeModalOpen(!isQrCodeModalOpen)}
          />
        </Modal>
        <Modal
          visible={
            quickAuthModalState.step !== CONNECT_QUICK_AUTH_STEPS[0].step
          }
          onDismiss={() => setQuickAuthModalState(CONNECT_QUICK_AUTH_STEPS[0])}
          contentContainerStyle={{ backgroundColor: "white", padding: 15 }}
        >
          <QuickAuth
            quickAuth={quickAuth}
            isConnecting={isConnecting}
            currSSID={currSSID}
            quickAuthModalState={quickAuthModalState}
            setQuickAuthModalState={setQuickAuthModalState}
            searchedDevices={searchedDevices}
          />
        </Modal>
      </Portal>
    </View>
  );
};

export default Connect;
