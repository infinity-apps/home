import React, {useEffect} from "react";
import {useDispatch, useSelector} from 'react-redux';
import {SafeAreaView, View} from 'react-native';
import { useFonts, NotoSans_300Light, NotoSans_400Regular, NotoSans_700Bold } from '@expo-google-fonts/noto-sans';
import { SignikaNegative_300Light, SignikaNegative_400Regular } from '@expo-google-fonts/signika-negative';

import Notifications from 'Components/Notifications';
import AppLoading from 'Components/AppLoading';
import { configAction } from 'Store/Config/actions';
import { identityAction } from 'Store/Identity/actions';
import Navigation from 'Navigation';
import Translate from 'Utils/Translations';
import * as SecureStore from "expo-secure-store";

export default function Index() {
  const dispatch = useDispatch();

  const identity = useSelector(({ identity }) => identity.identity);
  const _id = useSelector(({ identity }) => identity._id);
  const _token = useSelector(({ identity }) => identity._token);
  const config = useSelector(({ config }) => config);
  const availableTranslations = useSelector(({ config }) => config.availableTranslations);
  const globalLoader = useSelector(({ loader }) => loader.global);

  useEffect(() => {
    identityAction(dispatch).then(() => configAction(dispatch));
  }, []);

  const [fontsLoaded] = useFonts({
    NotoSans_300Light,
    NotoSans_400Regular,
    NotoSans_700Bold,
    SignikaNegative_300Light,
    SignikaNegative_400Regular,
  });

  module.__ = Translate.bind({ config });
  if (global) {
    global.__ = module.__;
  }

  const isLoading =
      !fontsLoaded || !identity || _id === 'UNSET' || _token === 'UNSET' || !availableTranslations; // @CHECK simplify?

  if (isLoading) {
    return <AppLoading />;
  }

  return (
    <SafeAreaView>
      <View width="100%" height="100%" position="relative">
        <Notifications />
        <Navigation />
      </View>
    </SafeAreaView>
  );
}