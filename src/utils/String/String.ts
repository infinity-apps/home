import { helperRegexp } from 'Utils/Regexp';

export const capitalizeFirstLetter = (string: string): string => string && string.charAt(0).toUpperCase() + string.slice(1);

export const removeSpaces = (string: string): string => string.replace(helperRegexp.space, '');

export const parseBool = (string: string): boolean => string !== undefined && string.toLowerCase() === 'true';

export const isJsonString = (str: string | Response) => {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }

    return true;
}