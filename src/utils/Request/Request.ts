// import config from '@base/config';
import { EXPO_PUBLIC_APP_NAME, EXPO_PUBLIC_API_IPV4, EXPO_PUBLIC_API_PORT, EXPO_PUBLIC_API_ENDPOINT, EXPO_PUBLIC_API_IS_SECURED, EXPO_PUBLIC_API_DEVICE_IPV4 } from '@env';
import { parseBool } from 'Utils/String';
import * as SecureStore from 'expo-secure-store';

// export const queryString = (params: { [key: string]: string }) =>
//   Object.keys(params)
//     .map((key) => `${key}=${params[key]}`)
//     .join('&');
// export const expressQueryString = (params: { [key: string]: string }) =>
//   Object.keys(params)
//     .map((key) => `${key}/${params[key]}`)
//     .join('/');

export const request = async (
    endpoint,
    payload,
    successCallback = () => {},
    errorCallback = () => {},
    callback = () => {}
) => {
    const _id = await SecureStore.getItemAsync('_id');
    const _token = await SecureStore.getItemAsync('_token');
    const user_id = payload?.user_id || (await SecureStore.getItemAsync('user_id')) || null;
    const url = `${
        parseBool(EXPO_PUBLIC_API_IS_SECURED) ? 'https' : 'http'
    }://${EXPO_PUBLIC_API_IPV4}:${EXPO_PUBLIC_API_PORT}/${EXPO_PUBLIC_API_ENDPOINT}/${endpoint}`;

    payload['application'] = EXPO_PUBLIC_APP_NAME;
    payload['_id'] = _id;
    payload['user_id'] = user_id;

    return await fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            ...(_token && { Authorization: _token }), // @CHECK Bearer? (problem in backend to validate) - Prio 1/10
        },
        body: JSON.stringify(payload),
    })
        .then(async (res) => {
            const jsonRes = await res.json();

            callback();

            if (res.ok) {
                // @TODO save in logs
                return jsonRes;
            } else {
                // @TODO save in (error) logs
                throw jsonRes;
            }
        })
        .then((res) => successCallback(res))
        .catch((err) => errorCallback(err));
};

export const requestDevice = async (
    endpoint,
    payload,
    successCallback = () => {},
    errorCallback = () => {},
    callback = () => {}
) => {
    const _id = await SecureStore.getItemAsync('_id');
    const _token = await SecureStore.getItemAsync('_token');
    const url = `http://${EXPO_PUBLIC_API_DEVICE_IPV4}/${endpoint}`;

    payload['_id'] = _id;

    return await fetch(url, {
        method: 'POST',
        headers: {
            Accept: 'application/json',
            'Content-Type': 'application/json',
            ...(_token && { Authorization: _token }), // @CHECK Bearer? (problem in backend to validate) - Prio 1/10
        },
        body: JSON.stringify(payload),
    })
        .then(async (res) => {
            const jsonRes = await res.json();
            alert('A' + JSON.stringify(jsonRes) + url);

            callback();

            if (jsonRes.ok) {
                // @TODO save in logs
                return jsonRes;
            } else {
                // @TODO save in (error) logs
                throw jsonRes;
            }
        })
        .then((res) => alert('B' + JSON.stringify(res) + url))
        .catch((err) => alert('C' + JSON.stringify(err) + url));
};