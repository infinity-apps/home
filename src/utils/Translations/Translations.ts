const Translate = function (code, replace) { // @TODO handle multiple replaces?
  const { translations, availableTranslations } = this.config;

  if (translations && availableTranslations) {
    const parsedTranslations = JSON.parse(translations[availableTranslations[0]]);
    let translatedText = parsedTranslations[code] || code;

    if (translatedText.includes('%s')) {
      translatedText = translatedText.replace('%s', replace);
      translatedText = parsedTranslations[translatedText] || translatedText;
    }

    return translatedText;
  }

  return code;
};

export default Translate;
