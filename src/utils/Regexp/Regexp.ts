export const regexp = {
  id: /^[a-zA-Z0-9]{6}$/,
  hash: /^[a-zA-Z0-9]{8}$/,
  // email: /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
  // phone: /^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$/,
  // password: /^(?=.*?[A-Z])(?=.*[a-z])(?=.*\d).{8,}$/, // >= 8 characters, at least one digit, uppercase and lowercase character
};

export const helperRegexp = {
  space: /\s/g,
}