/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {TouchableRipple} from 'react-native-paper'
import { Ionicons } from '@expo/vector-icons';

const HeaderLeft = (props) => {
  const {navigation} = props;

  return (
      <TouchableRipple
          onPress={() => navigation.openDrawer()}
          rippleColor="rgba(0, 0, 0, .2)"
          style={{padding: 12}}
      >
          <Ionicons name="md-menu-outline" size={30} color="black" />
      </TouchableRipple>
  );
};

export default HeaderLeft;
