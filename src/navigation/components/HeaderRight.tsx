/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

import React from 'react';
import {TouchableRipple} from 'react-native-paper'
import {View} from 'react-native';
import { Ionicons } from '@expo/vector-icons';

const HeaderRight = () => {
  return (
    <View style={{flexDirection: 'row'}}>
        <TouchableRipple
            onPress={() => console.log('Pressed')}
            rippleColor="rgba(0, 0, 0, .2)"
            style={{padding: 10}}
        >
            <Ionicons name="md-refresh-sharp" size={24} color="black" />
        </TouchableRipple>
        <TouchableRipple
            onPress={() => console.log('Pressed')}
            rippleColor="rgba(0, 0, 0, .2)"
            style={{padding: 10}}
        >
            <Ionicons name="notifications-outline" size={24} color="black" />
        </TouchableRipple>
    </View>
  );
};

export default HeaderRight;
