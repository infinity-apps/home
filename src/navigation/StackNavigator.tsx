import {useSelector} from "react-redux";
import {createNativeStackNavigator} from "@react-navigation/native-stack";

import ConnectScreen from 'Screens/Connect';
import HomeScreen from 'Screens/Home';

const Stack = createNativeStackNavigator();

const StackNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="Connect" component={ConnectScreen} options={{headerShown: false}} />
      <Stack.Screen name="Home" component={HomeScreen} options={{headerShown: false}} />
    </Stack.Navigator>
  );
};

export default StackNavigator;
