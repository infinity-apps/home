import React from "react";
import {DefaultTheme, NavigationContainer} from "@react-navigation/native";

import DrawerNavigator from "./DrawerNavigator";

const MyTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    background: 'white'
  },
};

const Navigation = () => {
  return (
    <NavigationContainer theme={ MyTheme }>
      <DrawerNavigator />
    </NavigationContainer>
  );
};

export default Navigation;
