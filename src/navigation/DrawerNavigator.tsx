import {Appbar, TouchableRipple, Button, List} from "react-native-paper";
import {Modal, View, Text} from 'react-native';
import {Ionicons} from "@expo/vector-icons";
import {
  createDrawerNavigator,
} from '@react-navigation/drawer';

import {buttonColors} from 'Constants/Colors';
import Logotype from 'Components/Logotype';
import NavigationHeader from './Header';
import StackNavigator from './StackNavigator'
import React from "react";

const Drawer = createDrawerNavigator();

const DrawerContent = (props) => {
    const { navigation } = props;
    const [isModalOpen, setIsModalOpen] = React.useState(false);

    return (
        <View style={{height: '100%'}}>
            <Appbar.Header style={{elevation: 0, backgroundColor: 'white'}}>
                <Appbar.Content title={(<TouchableRipple
                    onPress={() => navigation.closeDrawer()}
                    rippleColor="rgba(0, 0, 0, .2)"
                    style={{padding: 12}}
                >
                    <Ionicons name="md-close-outline" size={30} color="black"/>
                </TouchableRipple>)}/>
            </Appbar.Header>
            {/*<Divider />*/}
            <View style={{paddingHorizontal: 0, flex: 1}}>
                <TouchableRipple
                    onPress={() => setIsModalOpen(!isModalOpen)}
                    rippleColor="rgba(0, 0, 0, .2)"
                    style={{paddingHorizontal: 10, paddingVertical: 5}}
                >
                    <List.Item title="Authorize" titleStyle={{marginLeft: 5}} left={() => <Ionicons name="log-in-outline" size={22} color="black" style={{alignSelf: 'center'}} />} />
                </TouchableRipple>
                <TouchableRipple
                    onPress={() => navigation.closeDrawer()}
                    rippleColor="rgba(0, 0, 0, .2)"
                    style={{paddingHorizontal: 10, paddingVertical: 5}}
                >
                    <List.Item title="Devices" titleStyle={{marginLeft: 5}} left={() => <Ionicons name="hardware-chip-outline" size={22} color="black" style={{alignSelf: 'center'}} />} />
                </TouchableRipple>
                <TouchableRipple
                    onPress={() => navigation.closeDrawer()}
                    rippleColor="rgba(0, 0, 0, .2)"
                    style={{paddingHorizontal: 10, paddingVertical: 5}}
                >
                    <List.Item title="Settings" titleStyle={{marginLeft: 5}} left={() => <Ionicons name="settings-outline" size={22} color="black" style={{alignSelf: 'center'}} />} />
                </TouchableRipple>
                <TouchableRipple
                    onPress={() => navigation.closeDrawer()}
                    rippleColor="rgba(0, 0, 0, .2)"
                    style={{paddingHorizontal: 10, paddingVertical: 5}}
                >
                    <List.Item title="Lorem" titleStyle={{marginLeft: 5}} left={() => <Ionicons name="planet-outline" size={22} color="black" style={{alignSelf: 'center'}} />} />
                </TouchableRipple>
                <TouchableRipple
                    onPress={() => navigation.closeDrawer()}
                    rippleColor="rgba(0, 0, 0, .2)"
                    style={{paddingHorizontal: 10, paddingVertical: 5}}
                >
                    <List.Item title="Ipsum" titleStyle={{marginLeft: 5}} left={() => <Ionicons name="happy-outline" size={22} color="black" style={{alignSelf: 'center'}} />} />
                </TouchableRipple>
            </View>
            <View style={{padding: 20}}>
                {/*<Flex justifyContent="space-between" direction="row" style={{marginBottom: 20}}>*/}
                {/*    <Button color={buttonColors.primary} mode="contained" style={{width: '100%'}}>Logout</Button>*/}
                {/*</Flex>*/}
                <View style={{marginBottom: 20, padding: 20, justifyContent: "space-between", flexDirection: "row"}}>
                    <Button color={buttonColors.secondary} mode="contained" style={{width: '48%'}}>Contact</Button>
                    <Button color={buttonColors.secondary} mode="contained" style={{width: '48%'}}>Help</Button>
                </View>
                <View style={{width: '50%', height: 1, left: '25%', backgroundColor: '#666'}}/>
                <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center', marginTop: 20}}>
                    <Logotype showText={false} />
                    <View style={{flexDirection: 'column', alignItems: 'flex-end', marginLeft: 6}}>
                        <Text style={{fontSize: 11, fontWeight: 'bold', letterSpacing: -0.5}}>v. 0.244.7</Text>
                        <Text style={{marginTop: -3, fontSize: 11, fontWeight: 'bold', letterSpacing: -0.5}}>2022.06.2</Text>
                    </View>
                </View>
            </View>
            {/*<Modal isOpen={isModalOpen} onClose={() => setIsModalOpen(false)} avoidKeyboard justifyContent="center" size="lg">*/}
            {/*    <Modal.Content>*/}
            {/*        <Modal.CloseButton />*/}
            {/*        <Modal.Header>Authorize your app</Modal.Header>*/}
            {/*        <Modal.Body>*/}
            {/*            To access all features - please authorize your app. You can do it by e-mail address or phone number.*/}
            {/*        </Modal.Body>*/}
            {/*        <Modal.Footer>*/}
            {/*            <Button onPress={() => {*/}
            {/*                setIsModalOpen(false);*/}
            {/*            }}>*/}
            {/*                Authorize*/}
            {/*            </Button>*/}
            {/*        </Modal.Footer>*/}
            {/*    </Modal.Content>*/}
            {/*</Modal>*/}
        </View>
    );
};

const DrawerNavigator = (props) => {
  return (
    <Drawer.Navigator drawerContent={(props) => <DrawerContent {...props} />}>
      <Drawer.Screen name="StackNavigator" component={StackNavigator} options={{header: (props) => <NavigationHeader {...props} />}} />
    </Drawer.Navigator>
  );
};

export default DrawerNavigator;
