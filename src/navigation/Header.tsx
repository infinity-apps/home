import {Appbar} from 'react-native-paper';

import Logotype from 'Components/Logotype';
import HeaderLeft from './components/HeaderLeft'
import HeaderRight from './components/HeaderRight'

const NavigationHeader = (props) => {
  const { navigation } = props;

  return (
    <Appbar.Header style={{backgroundColor: 'white'}}>
      <Appbar.Content title={<HeaderLeft {...props} />} onClick={() => navigation.openDrawer()} />
      <Appbar.Content title={<Logotype {...props} />} />
      <Appbar.Content title={<HeaderRight {...props} />} titleStyle={{marginLeft: 'auto'}}/>
    </Appbar.Header>
  );
};

export default NavigationHeader;