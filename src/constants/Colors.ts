import { NOTIFICATION_TYPE_SUCCESS, NOTIFICATION_TYPE_INFO, NOTIFICATION_TYPE_WARNING, NOTIFICATION_TYPE_ERROR } from 'Store/Notification/types';

export const notificationColors = {
    [NOTIFICATION_TYPE_SUCCESS]: '#0d6410',
    [NOTIFICATION_TYPE_INFO]: '#14428e',
    [NOTIFICATION_TYPE_WARNING]: '#867706',
    [NOTIFICATION_TYPE_ERROR]: '#860c0c'
};

export const buttonColors = {
    primary: '#440260',
    secondary: '#1b3e77'
};