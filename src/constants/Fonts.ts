export const fonts = {
    primaryLight: 'NotoSans_300Light',
    primary: 'NotoSans_400Regular',
    primaryBold: 'NotoSans_700Bold',
    secondary: 'SignikaNegative_400Regular',
    secondaryLight: 'SignikaNegative_300Light',
}