# README #

Infinity Apps @ 2021
###

### Requirements ###
- npm version >= 12

### Starting the project ###

``` bash
# install dependencies
npm install

# GUI
npm run start

# web view emulator 
npm run web

# android device with developer mode enabled should be connected by USB
npm run android

# ios device with developer mode enabled should be connected by USB
npm run ios
```